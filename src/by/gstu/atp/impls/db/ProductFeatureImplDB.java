package by.gstu.atp.impls.db;

import by.gstu.atp.factories.ProductFeatureFactory;
import by.gstu.atp.interfaces.AbstractBeanImplDB;

public class ProductFeatureImplDB extends AbstractBeanImplDB {

	public ProductFeatureImplDB() {
		super(new ProductFeatureFactory());
	}

}
