package by.gstu.atp.impls.db;

public final class ConstantsQueries {

	public static final String INSERT_UNIT = "INSERT INTO units (fullName, shortName) VALUES (?, ?)";
	public static final String INSERT_PRODUCT_TYPE = "INSERT INTO product_types (name) VALUES (?)";
	public static final String INSERT_PRODUCT_KIND = "INSERT INTO product_kinds (name) VALUES (?)";
	public static final String INSERT_PRODUCT_FEATURE = "INSERT INTO product_features (name) VALUES (?)";
	public static final String INSERT_PRODUCT_NAME = "INSERT INTO product_names ("
			+ "nameCode, name, designation, typeCode, kindCode, featureCode, unitCode, count) VALUES (?,?,?,?,?,?,?,?)";
	public static final String INSERT_APP_RATE = "INSERT INTO application_rates ("
			+ "nameCode, materialCode, consumptionRate, wasteRate) VALUES (?, ?, ?, ?)";
	public static final String INSERT_MATERIAL = "INSERT INTO materials (materialCode, name, unitCode) VALUES (?, ?, ?)";
	public static final String INSERT_COMPOSITE = "INSERT INTO structure (main, sub, count) VALUES (?, ?, ?)";

	public static final String SELECT_UNIT = "SELECT fullName, shortName, unitCode FROM units WHERE (unitCode = ?)";
	public static final String SELECT_PRODUCT_TYPE = "SELECT name FROM product_types WHERE (typeCode = ?)";
	public static final String SELECT_PRODUCT_KIND = "SELECT name FROM product_kinds WHERE (kindCode = ?)";
	public static final String SELECT_PRODUCT_FEATURE = "SELECT name FROM product_features WHERE (featureCode = ?)";
	public static final String SELECT_PRODUCT_NAME = "SELECT name, designation, typeCode, kindCode, featureCode, unitCode, count "
			+ "FROM product_names WHERE (nameCode = ?)";
	public static final String SELECT_APP_RATE = "SELECT nameCode, materialCode, consumptionRate, wasteRate "
			+ "FROM application_rates WHERE (appRateCode = ?)";
	public static final String SELECT_MATERIAL = "SELECT name, unitCode FROM materials WHERE (materialCode = ?)";
	public static final String SELECT_COMPOSITE = "SELECT id, main, sub, count FROM structure WHERE id = ?";
	
	public static final String SELECT_APP_RATE_ALL = "select application_rates.appRateCode, application_rates.nameCode, application_rates.materialCode, consumptionRate, wasteRate, product_names.name, product_names.designation, product_names.typeCode, product_names.kindCode, product_names.featureCode, product_names.unitCode, product_names.count, product_types.name, "
			+ "product_kinds.name, product_features.name, units.fullName, units.shortName, materials.name FROM application_rates INNER JOIN product_names ON application_rates.nameCode = product_names.nameCode INNER JOIN product_types ON product_names.typeCode = product_types.typeCode "
			+ "INNER JOIN product_kinds ON product_names.kindCode = product_kinds.kindCode INNER JOIN product_features ON product_names.featureCode = product_features.featureCode INNER JOIN units ON product_names.unitCode = units.unitCode INNER JOIN materials ON application_rates.materialCode = materials.materialCode "
			+ "WHERE (appRateCode = ?)";
	public static final String SELECT_MATERIAL_ALL = "SELECT name, materials.unitCode, fullName, shortName FROM materials "
			+ "INNER JOIN units ON materials.unitCode = units.unitCode WHERE (materialCode = ?)";
	public static final String SELECT_PRODUCT_NAME_ALL = "SELECT product_names.name, designation, product_names.typeCode, product_names.kindCode, product_names.featureCode, product_names.unitCode, count, product_types.name, product_kinds.name, product_features.name, units.fullName, units.shortName "
			+ "FROM product_names INNER JOIN product_types ON product_names.typeCode = product_types.typeCode INNER JOIN product_kinds ON product_names.kindCode = product_kinds.kindCode INNER JOIN product_features ON product_names.featureCode = product_features.featureCode INNER JOIN units ON product_names.unitCode = units.unitCode WHERE (nameCode = ?)";
	public static final String SELECT_COMPOSITE_ALL = "SELECT id, structure.main, product_names.name, structure.sub, "
			+ "structure.count FROM structure INNER JOIN product_names ON structure.main = product_names.nameCode";
	public static final String SELECT_COMPOSITE_SUB_NAMES = "SELECT sub, product_names.name FROM structure "
			+ "INNER JOIN product_names ON structure.sub = product_names.nameCode";
	public static final String SELECT_PRODUCTS_ALL ="SELECT structure.id, structure.main, product_names.name, product_names.kindCode, "
			+ "structure.count FROM structure INNER JOIN product_names ON structure.main = "
			+ "product_names.nameCode WHERE sub = ?";

	public static final String SELECT_UNITS = "SELECT * FROM units";
	public static final String SELECT_PRODUCT_TYPES = "SELECT * FROM product_types";
	public static final String SELECT_PRODUCT_KINDS = "SELECT * FROM product_kinds";
	public static final String SELECT_PRODUCT_FEATURES = "SELECT * FROM product_features";
	public static final String SELECT_PRODUCT_NAMES = "SELECT * FROM product_names";
	public static final String SELECT_APP_RATES = "SELECT * FROM application_rates";
	public static final String SELECT_MATERIALS = "SELECT * FROM materials";
	public static final String SELECT_APP_RATES_ALL = "select application_rates.appRateCode, application_rates.nameCode, application_rates.materialCode, consumptionRate, wasteRate, product_names.name, product_names.designation, product_names.typeCode, product_names.kindCode, product_names.featureCode, product_names.unitCode, product_names.count, product_types.name, "
			+ "product_kinds.name, product_features.name, units.fullName, units.shortName, materials.name FROM application_rates INNER JOIN product_names ON application_rates.nameCode = product_names.nameCode INNER JOIN product_types ON product_names.typeCode = product_types.typeCode "
			+ "INNER JOIN product_kinds ON product_names.kindCode = product_kinds.kindCode INNER JOIN product_features ON product_names.featureCode = product_features.featureCode INNER JOIN units ON product_names.unitCode = units.unitCode INNER JOIN materials ON application_rates.materialCode = materials.materialCode";
	public static final String SELECT_MATERIALS_ALL = "SELECT name, materials.unitCode, fullName, shortName, materialCode FROM materials "
			+ "INNER JOIN units ON materials.unitCode = units.unitCode";
	public static final String SELECT_PRODUCT_NAMES_ALL = "SELECT product_names.name, designation, product_names.typeCode, product_names.kindCode, product_names.featureCode, product_names.unitCode, count, product_types.name, product_kinds.name, product_features.name, units.fullName, units.shortName, nameCode "
			+ "FROM product_names INNER JOIN product_types ON product_names.typeCode = product_types.typeCode INNER JOIN product_kinds ON product_names.kindCode = product_kinds.kindCode INNER JOIN product_features ON product_names.featureCode = product_features.featureCode INNER JOIN units ON product_names.unitCode = units.unitCode";

	public static final String UPDATE_UNIT = "UPDATE units SET fullName = ?, shortName = ? WHERE (unitCode = ?)";
	public static final String UPDATE_PRODUCT_TYPE = "UPDATE product_types SET name = ? WHERE (typeCode = ?)";
	public static final String UPDATE_PRODUCT_KIND = "UPDATE product_kinds SET name = ? WHERE (kindCode = ?)";
	public static final String UPDATE_PRODUCT_FEATURE = "UPDATE product_features SET name = ? WHERE (featureCode = ?)";
	public static final String UPDATE_PRODUCT_NAME = "UPDATE product_names SET name = ?, designation = ?, "
			+ "typeCode = ?, kindCode = ?, featureCode = ?, unitCode = ?, count = ? WHERE (nameCode = ?)";
	public static final String UPDATE_APP_RATE = "UPDATE application_rates SET nameCode = ?, materialCode = ?, "
			+ "consumptionRate = ?, wasteRate = ? WHERE (appRateCode = ?)";
	public static final String UPDATE_MATERIAL = "UPDATE materials SET name = ?, unitCode = ? WHERE (materialCode = ?)";
	public static final String UPDATE_COMPOSITE = "UPDATE structure SET main = ? , sub = ?, count = ? WHERE id = ?";

	public static final String DELETE_UNIT = "DELETE FROM units WHERE (unitCode = ?)";
	public static final String DELETE_PRODUCT_TYPE = "DELETE FROM product_types WHERE (typeCode = ?)";
	public static final String DELETE_PRODUCT_KIND = "DELETE FROM product_kinds WHERE (kindCode = ?)";
	public static final String DELETE_PRODUCT_FEATURE = "DELETE FROM product_features WHERE (featureCode = ?)";
	public static final String DELETE_PRODUCT_NAME = "DELETE FROM product_names WHERE (nameCode = ?)";
	public static final String DELETE_APP_RATE = "DELETE FROM application_rates WHERE (appRateCode = ?)";
	public static final String DELETE_MATERIAL = "DELETE FROM materials WHERE (materialCode = ?)";
	public static final String DELETE_COMPOSITE = "DELETE FROM structure WHERE (id = ?)";

	private ConstantsQueries() { }

}
