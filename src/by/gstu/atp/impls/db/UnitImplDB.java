package by.gstu.atp.impls.db;

import by.gstu.atp.factories.UnitFactory;
import by.gstu.atp.interfaces.AbstractBeanImplDB;

public class UnitImplDB extends AbstractBeanImplDB {

	public UnitImplDB() {
		super(new UnitFactory());
	}

}
