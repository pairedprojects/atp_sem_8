package by.gstu.atp.impls.db;

import by.gstu.atp.factories.ProductNameFactory;
import by.gstu.atp.interfaces.AbstractBeanImplDB;

public class ProductNameImplDB extends AbstractBeanImplDB {

	public ProductNameImplDB() {
		super(new ProductNameFactory());
	}

}
