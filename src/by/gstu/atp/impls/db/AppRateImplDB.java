package by.gstu.atp.impls.db;

import by.gstu.atp.factories.AppRateFactory;
import by.gstu.atp.interfaces.AbstractBeanImplDB;

public class AppRateImplDB extends AbstractBeanImplDB{

	public AppRateImplDB() {
		super(new AppRateFactory());
	}

}
