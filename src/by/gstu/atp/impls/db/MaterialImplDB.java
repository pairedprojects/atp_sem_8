package by.gstu.atp.impls.db;

import by.gstu.atp.factories.MaterialFactory;
import by.gstu.atp.interfaces.AbstractBeanImplDB;

public class MaterialImplDB extends AbstractBeanImplDB {

	public MaterialImplDB() {
		super(new MaterialFactory());
	}

}
