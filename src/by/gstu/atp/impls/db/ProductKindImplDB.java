package by.gstu.atp.impls.db;

import by.gstu.atp.factories.ProductKindFactory;
import by.gstu.atp.interfaces.AbstractBeanImplDB;

public class ProductKindImplDB extends AbstractBeanImplDB {

	public ProductKindImplDB() {
		super(new ProductKindFactory());
	}
		
}
