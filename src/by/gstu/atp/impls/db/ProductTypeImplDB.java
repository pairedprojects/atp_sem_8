package by.gstu.atp.impls.db;

import by.gstu.atp.factories.ProductTypeFactory;
import by.gstu.atp.interfaces.AbstractBeanImplDB;

public class ProductTypeImplDB extends AbstractBeanImplDB {

	public ProductTypeImplDB() {
		super(new ProductTypeFactory());
	}

}
