package by.gstu.atp.impls.db;

import by.gstu.atp.factories.CompositeFactory;
import by.gstu.atp.interfaces.AbstractBeanImplDB;

public class CompositeImplDB extends AbstractBeanImplDB {

	public CompositeImplDB() {
		super(new CompositeFactory());
	}

}
