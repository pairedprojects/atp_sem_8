package by.gstu.atp.beans;

import by.gstu.atp.interfaces.IBean;

public class ApplicationRate implements IBean {

	private long code;
	private ProductName name;
	private Material material;
	private double consumptionRate;
	private double wasteRate;

	public ApplicationRate() { }

	public ApplicationRate(ProductName name, Material material,
			double consumptionRate, double wasteRate) {
		this(Constants.DEFAULT_ID, name, material, consumptionRate, wasteRate);
	}

	public ApplicationRate(long code, ProductName name, Material material,
			double consumptionRate, double wasteRate) {
		this.code = code;
		this.name = name;
		this.material = material;
		this.consumptionRate = consumptionRate;
		this.wasteRate = wasteRate;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public ProductName getName() {
		return name;
	}

	public void setName(ProductName name) {
		this.name = name;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public double getConsumptionRate() {
		return consumptionRate;
	}

	public void setConsumptionRate(double consumptionRate) {
		this.consumptionRate = consumptionRate;
	}

	public double getWasteRate() {
		return wasteRate;
	}

	public void setWasteRate(double wasteRate) {
		this.wasteRate = wasteRate;
	}

	@Override
	public String[] getHeader() {
		return new String[] { "���", "������������", "��������", "�������",
				"������" };
	}

	@Override
	public String[] getContent() {
		return new String[] { "" + getCode(), getName().getName(),
				getMaterial().getName(), "" + getConsumptionRate(),
				"" + getWasteRate() };
	}

	@Override
	public String getHtmlValue() {
		return "" + getCode();
	}

	@Override
	public String getHtmlName() {
		return "" + getCode();
	}

	@Override
	public String[] getFields() {
		return new String[] { "" + getName().getCode(),
				"" + getMaterial().getCode(), "" + getConsumptionRate(),
				"" + getWasteRate() };
	}

}
