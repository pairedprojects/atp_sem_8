package by.gstu.atp.beans.tree;

import java.util.List;

import by.gstu.atp.beans.Composite;

public class Node {

	public Node parent;
	public List<Node> nodes;
	public Composite comp;
	public boolean isCalc;

	public Node() {
	}

	public Node(Composite c) {
		comp = c;
	}

	public Node(Composite c, Node parent) {
		this(c);
		this.parent = parent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comp == null) ? 0 : comp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (comp == null) {
			if (other.comp != null)
				return false;
		} else if (!comp.equals(other.comp))
			return false;
		return true;
	}

}
