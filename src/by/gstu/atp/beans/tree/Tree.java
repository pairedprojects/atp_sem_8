package by.gstu.atp.beans.tree;

import java.util.ArrayList;
import java.util.List;

import by.gstu.atp.beans.Composite;

public class Tree {

	private Node root;
	private Node cur;

	public Tree() {
	}

	public Tree(Composite comp) {
		root = new Node(comp);
		cur = root;
	}

	public Node getRoot() {
		return root;
	}

	public Node getCur() {
		return cur;
	}

	public List<Node> getCurNodes() {
		return cur.nodes;
	}

	public void add(List<Composite> composites) {
		if (composites == null) {
			cur.nodes = null;
			return;
		}
		List<Node> nodes = new ArrayList<>();
		for (Composite comp : composites) {
			nodes.add(new Node(comp, cur));
		}
		cur.nodes = nodes;
	}

	public boolean nextBrunch(Node c) {
		if (cur.nodes != null) {
			int index = cur.nodes.indexOf(c);
			if (index != -1) {
				cur = cur.nodes.get(index);
				return true;
			}
		}
		return false;
	}

	public boolean prevBrunch() {
		if (cur.parent != null) {
			cur = cur.parent;
			return true;
		}
		return false;
	}

	public int calc(Node n) {
		int count = rCalc();
		cur = n;
		return count;
	}

	private int rCalc() {
		if (cur.parent != null) {
			int cCount = cur.comp.getCount();
			cur = cur.parent;
			return cCount * rCalc();
		}
		return 1;
	}

}
