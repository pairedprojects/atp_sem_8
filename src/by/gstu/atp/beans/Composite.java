package by.gstu.atp.beans;

import by.gstu.atp.interfaces.IBean;

public class Composite implements IBean {

	private long code;
	private ProductName main;
	private ProductName sub;
	private int count;

	public Composite() {
	}

	public Composite(ProductName main, ProductName sub, int count) {
		this(Constants.DEFAULT_ID, main, sub, count);
	}

	public Composite(long code, ProductName main, ProductName sub, int count) {
		this.code = code;
		this.main = main;
		this.sub = sub;
		this.count = count;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public ProductName getMain() {
		return main;
	}

	public void setMain(ProductName main) {
		this.main = main;
	}

	public ProductName getSub() {
		return sub;
	}

	public void setSub(ProductName sub) {
		this.sub = sub;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String[] getHeader() {
		return new String[] { "���", "�������/��������� �������",
				"��������� �������/������", "����������" };
	}

	@Override
	public String[] getContent() {
		return new String[] { "" + getCode(), getMain().getName(),
				getSub().getName(), "" + getCount() };
	}

	@Override
	public String getHtmlValue() {
		return "" + getCode();
	}

	@Override
	public String getHtmlName() {
		return "" + getCode();
	}

	@Override
	public String[] getFields() {
		return new String[] { "" + getMain().getCode(),
				"" + getSub().getCode(), "" + getCount() };
	}

	@Override
	public String toString() {
		return "C[" + main + "; " + sub + "; " + count + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((main == null) ? 0 : main.hashCode());
		result = prime * result + ((sub == null) ? 0 : sub.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Composite other = (Composite) obj;
		if (main == null) {
			if (other.main != null)
				return false;
		} else if (!main.equals(other.main))
			return false;
		if (sub == null) {
			if (other.sub != null)
				return false;
		} else if (!sub.equals(other.sub))
			return false;
		return true;
	}

}
