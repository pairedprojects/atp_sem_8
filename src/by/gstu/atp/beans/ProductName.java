package by.gstu.atp.beans;

import by.gstu.atp.interfaces.IBean;

public class ProductName implements IBean {

	private long code;
	private String name;
	private String designation;
	private ProductType type;
	private ProductKind kind;
	private ProductFeature feature;
	private Unit unit;
	private int count;

	public ProductName() {
	}

	public ProductName(String name, String designation, ProductType type,
			ProductKind kind, ProductFeature feature, Unit unit, int count) {
		this(Constants.DEFAULT_ID, name, designation, type, kind, feature,
				unit, count);
	}

	public ProductName(long code, String name, String designation,
			ProductType type, ProductKind kind, ProductFeature feature,
			Unit unit, int count) {
		this.code = code;
		this.name = name;
		this.designation = designation;
		this.type = type;
		this.kind = kind;
		this.feature = feature;
		this.unit = unit;
		this.count = count;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	public ProductKind getKind() {
		return kind;
	}

	public void setKind(ProductKind kind) {
		this.kind = kind;
	}

	public ProductFeature getFeature() {
		return feature;
	}

	public void setFeature(ProductFeature feature) {
		this.feature = feature;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String[] getHeader() {
		return new String[] { "���", "������������", "�����������", "���",
				"���", "�������", "��. ���.", "���-��" };
	}

	@Override
	public String[] getContent() {
		return new String[] { "" + getCode(), getName(), getDesignation(),
				getType().getName(), getKind().getName(),
				getFeature().getName(), getUnit().getShortName(),
				"" + getCount() };
	}

	@Override
	public String getHtmlValue() {
		return "" + getCode();
	}

	@Override
	public String getHtmlName() {
		return getName();
	}

	@Override
	public String[] getFields() {
		return new String[] { "" + getCode(), getName(), getDesignation(),
				"" + getType().getCode(), "" + getKind().getCode(),
				"" + getFeature().getCode(), "" + getUnit().getCode(),
				"" + getCount() };
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (code ^ (code >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductName other = (ProductName) obj;
		if (code != other.code)
			return false;
		return true;
	}

}
