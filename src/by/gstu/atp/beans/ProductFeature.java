package by.gstu.atp.beans;

import by.gstu.atp.interfaces.IBean;

public class ProductFeature implements IBean {

	private long code;
	private String name;

	public ProductFeature() { }

	public ProductFeature(String name) {
		this(Constants.DEFAULT_ID, name);
	}
	
	public ProductFeature(long code, String name) {
		this.code = code;
		this.name = name;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String[] getHeader() {
		return new String[] { "���", "������������" };
	}

	@Override
	public String[] getContent() {
		return new String[] { "" + getCode(), getName() };
	}
	
	@Override
	public String getHtmlValue() {
		return "" + getCode();
	}

	@Override
	public String getHtmlName() {
		return getName();
	}

	@Override
	public String[] getFields() {
		return new String[] { getName() };
	}

}
