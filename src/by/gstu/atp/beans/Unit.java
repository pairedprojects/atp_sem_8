package by.gstu.atp.beans;

import by.gstu.atp.interfaces.IBean;

public class Unit implements IBean {

	private long code;
	private String fullName;
	private String shortName;

	public Unit() { }

	public Unit(long code, String fullName, String shortName) {
		this.code = code;
		this.fullName = fullName;
		this.shortName = shortName;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	@Override
	public String[] getHeader() {
		return new String[] { "Код", "Полное наименование",
				"Краткое наименование" };
	}

	@Override
	public String[] getContent() {
		return new String[] { "" + getCode(), getFullName(), getShortName() };
	}

	@Override
	public String getHtmlValue() {
		return "" + getCode();
	}

	@Override
	public String getHtmlName() {
		return getFullName();
	}

	@Override
	public String[] getFields() {
		return new String[] { getFullName(), getShortName() };
	}

}
