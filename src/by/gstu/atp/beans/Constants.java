package by.gstu.atp.beans;

final class Constants {
	
	public static final long DEFAULT_ID = -1L;
	
	private Constants() { }
}
