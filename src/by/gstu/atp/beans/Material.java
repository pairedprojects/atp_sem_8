package by.gstu.atp.beans;

import by.gstu.atp.interfaces.IBean;

public class Material implements IBean {

	private long code;
	private String name;
	private Unit unit;

	public Material() { }

	public Material(long code, String name, Unit unit) {
		this.code = code;
		this.name = name;
		this.unit = unit;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Override
	public String[] getHeader() {
		return new String[] { "���", "������������", "��. ���." };
	}

	@Override
	public String[] getContent() {
		return new String[] { "" + getCode(), getName(),
				getUnit().getFullName() };
	}

	@Override
	public String getHtmlValue() {
		return "" + getCode();
	}

	@Override
	public String getHtmlName() {
		return getName();
	}

	@Override
	public String[] getFields() {
		return new String[] { "" + getCode(), getName(),
				"" + getUnit().getCode() };
	}

}
