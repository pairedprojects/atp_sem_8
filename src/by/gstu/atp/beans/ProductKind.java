package by.gstu.atp.beans;

import by.gstu.atp.interfaces.IBean;

public class ProductKind implements IBean {

	private long code;
	private String name;

	public ProductKind() { }

	public ProductKind(long id) {
		this(id, "");
	}

	public ProductKind(String name) {
		this(Constants.DEFAULT_ID, name);
	}

	public ProductKind(long code, String name) {
		this.code = code;
		this.name = name;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String[] getHeader() {
		return new String[] { "���", "������������" };
	}

	@Override
	public String[] getContent() {
		return new String[] { "" + getCode(), getName() };
	}

	@Override
	public String getHtmlValue() {
		return "" + getCode();
	}

	@Override
	public String getHtmlName() {
		return getName();
	}

	@Override
	public String[] getFields() {
		return new String[] { getName() };
	}

}
