package by.gstu.atp.beans;

import java.util.List;

import by.gstu.atp.interfaces.IBean;

public class HtmlField {

	private String name;
	private String type;
	private String htmlName;
	private List<IBean> values;

	public HtmlField(String name, String type, String htmlName,
			List<IBean> values) {
		this.name = name;
		this.type = type;
		this.htmlName = htmlName;
		this.values = values;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<IBean> getValues() {
		return values;
	}

	public void setValues(List<IBean> values) {
		this.values = values;
	}

	public String getHtmlName() {
		return htmlName;
	}

	public void setHtmlName(String htmlName) {
		this.htmlName = htmlName;
	}

}
