package by.gstu.atp.helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.impls.db.ConstantsDB;

public final class DatabaseUtils {

	static {
		try {
			Class.forName(ConstantsDB.DB_DRIVER_CLASS_NAME);
		} catch (ClassNotFoundException e) {
			throw new DaoException("Database driver not found.", e);
		}
	}

	private DatabaseUtils() { }

	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(ConstantsDB.DB_URL,
				ConstantsDB.DB_USER, ConstantsDB.DB_PASSWORD);
	}

}
