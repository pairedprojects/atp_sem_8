package by.gstu.atp.helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import by.gstu.atp.beans.Composite;
import by.gstu.atp.beans.ProductKind;
import by.gstu.atp.beans.ProductName;
import by.gstu.atp.beans.tree.Node;
import by.gstu.atp.beans.tree.Tree;
import by.gstu.atp.impls.db.ConstantsQueries;
import by.gstu.atp.impls.db.ProductNameImplDB;

public class SearchUtils {

	private List<Composite> list;
	private static ProductName initDetail;
	private Tree decisionTree;
	private Map<ProductName, Integer> fullApp;

	public SearchUtils() {
	}

	public List<Composite> getFinded() {
		return list;
	}

	public ProductName getRequired() {
		return initDetail;
	}

	private void buildTree(Node node) {
		if (node.nodes == null) {
			return;
		}
		for (Node n : node.nodes) {
			List<Composite> list = getDetailsForSub(n.comp);
			if (decisionTree.nextBrunch(n)) {
				decisionTree.add(list);
				buildTree(n);
				decisionTree.prevBrunch();
			}
		}
	}

	private void createDecision(Node node) {
		for (Node n : node.nodes) {
			if (decisionTree.nextBrunch(n)) {
				if (!decisionTree.getCur().isCalc) {
					ProductName pn = decisionTree.getCur().comp.getMain();
					int count = decisionTree.calc(decisionTree.getCur());
					if (fullApp.containsKey(pn)) {
						int prev = fullApp.get(pn);
						fullApp.put(pn, count + prev);
					} else {
						fullApp.put(pn, count);
					}
					decisionTree.getCur().isCalc = true;
				}
				if (decisionTree.getCur().nodes != null) {
					createDecision(decisionTree.getCur());
				}
				decisionTree.prevBrunch();
			}
		}
	}

	public void find(long id) {
		initDetail = (ProductName) new ProductNameImplDB().get(id);
		Composite start = new Composite(initDetail, null, 0);

		decisionTree = new Tree(start);
		fullApp = new HashMap<>();

		List<Composite> init = getDetailsForSub(start);
		if (init == null) {
			return;
		}
		decisionTree.add(getDetailsForSub(start));
		buildTree(decisionTree.getRoot());
		createDecision(decisionTree.getRoot());

		list = new ArrayList<>();
		for (Entry<ProductName, Integer> item : fullApp.entrySet()) {
			Composite c = new Composite(item.getKey(), initDetail,
					item.getValue());
			list.add(c);
		}
	}

	private List<Composite> getDetailsForSub(Composite comp) {
		List<Composite> result = new ArrayList<>();
		try (Connection con = DatabaseUtils.getConnection();
				PreparedStatement select = con
						.prepareStatement(ConstantsQueries.SELECT_PRODUCTS_ALL)) {
			select.setLong(1, comp.getMain().getCode());
			try (ResultSet rs = select.executeQuery()) {
				while (rs.next()) {
					long id = rs.getLong(1);
					long mainCode = rs.getLong(2);
					String mainName = rs.getString(3);
					long mainKind = rs.getLong(4);
					int count = rs.getInt(5);

					ProductName main = new ProductName();
					main.setCode(mainCode);
					main.setName(mainName);
					main.setKind(new ProductKind(mainKind));
					Composite c = new Composite(id, main, comp.getMain(), count);
					result.add(c);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result.size() == 0 ? null : result;
	}

	private void setCellsStyle(Cell[] cells, CellStyle style) {
		for (Cell cell : cells) {
			cell.setCellStyle(style);
		}
	}

	public Workbook formSheet(List<Composite> list) {
		final String SHEET_NAME = "details_total_sheet";

		Workbook book = new HSSFWorkbook();
		Sheet sheet = book.createSheet(SHEET_NAME);

		CellStyle headerStyle = book.createCellStyle();
		headerStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
		headerStyle.setBorderBottom(CellStyle.BORDER_MEDIUM);
		headerStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);
		headerStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
		headerStyle.setFillBackgroundColor(IndexedColors.LIGHT_BLUE.getIndex());

		Row rowHeader = sheet.createRow((short) 0);

		Cell c0 = rowHeader.createCell(0);
		c0.setCellValue("������������ ��/�������");
		sheet.autoSizeColumn(0);

		Cell c1 = rowHeader.createCell(1);
		c1.setCellValue("���������� ������� " + initDetail.getName()
				+ ", �������� � ��/�������");
		sheet.autoSizeColumn(1);

		setCellsStyle(new Cell[] { c0, c1 }, headerStyle);

		CellStyle cStyle = book.createCellStyle();
		cStyle.setBorderLeft(CellStyle.BORDER_MEDIUM);
		cStyle.setBorderRight(CellStyle.BORDER_MEDIUM);
		cStyle.setBorderBottom(CellStyle.BORDER_THIN);

		Row row = null;
		for (int i = 0; i < list.size(); i++) {
			int rowNumber = i + 1;
			row = sheet.createRow((char) rowNumber);
			c0 = row.createCell(0);
			c1 = row.createCell(1);
			Composite detail = list.get(i);
			c0.setCellValue(detail.getMain().getName());
			c1.setCellValue(detail.getCount());
			setCellsStyle(new Cell[] { c0, c1 }, cStyle);
		}
		return book;
	}

}
