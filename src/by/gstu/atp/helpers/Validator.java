package by.gstu.atp.helpers;

import by.gstu.atp.beans.ProductKind;
import by.gstu.atp.interfaces.IBean;

public final class Validator {

	private Validator() { }

	public static IBean[] checkIds(String[] ids) {
		if (ids == null) {
			return new IBean[0];
		}

		IBean[] result = new IBean[ids.length];
		for (int i = 0; i < ids.length; i++) {
			try {
				result[i] = new ProductKind(Long.parseLong(ids[i]));
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Id = '" + ids[i]
						+ "' can't be converted to LONG.");
			}
		}
		return result;
	}
	
	public static String checkReqParam(String param) {
		if (param == null || "".equals(param)) {
			throw new IllegalArgumentException("Request parameter was absent.");
		}
		return param;
	}

}
