package by.gstu.atp.factories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.gstu.atp.beans.ProductType;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.impls.db.ConstantsQueries;
import by.gstu.atp.interfaces.AbstractBeanFactory;
import by.gstu.atp.interfaces.IBean;

public class ProductTypeFactory extends AbstractBeanFactory {

	@Override
	public List<IBean> getBeansFromDb(PreparedStatement select)
			throws SQLException {
		List<IBean> types = new ArrayList<>();
		try (ResultSet rs = select.executeQuery()) {
			while (rs.next()) {
				try {
					long id = rs.getLong(1);
					String name = rs.getString(2);
					types.add(new ProductType(id, name));
				} catch (SQLException e) {
					throw new DaoException("Error reading product's type. Read types: "
							+ types.size() + ". Last successfully read type: "
							+ types.get(types.size() - 1));
				}
			}
			return types;
		}
	}

	@Override
	public IBean getBeanFromDb(long id, PreparedStatement select)
			throws SQLException {
		select.setLong(1, id);
		try (ResultSet rs = select.executeQuery()) {
			if (rs.next()) {
				String name = rs.getString(1);
				return new ProductType(id, name);
			}
		}
		throw new DaoException("Product type with id = '" + id
				+ "' is not found");
	}

	@Override
	public void setBeanToDb(IBean bean, PreparedStatement insert)
			throws SQLException {
		ProductType type = (ProductType) bean;
		insert.setString(1, type.getName());
		insert.executeUpdate();
	}

	@Override
	public void updateBeanInDb(IBean bean, PreparedStatement update)
			throws SQLException {
		ProductType type = (ProductType) bean;
		update.setString(1, type.getName());
		update.setLong(2, type.getCode());
		update.executeUpdate();
	}

	@Override
	public String getInsertQuery() {
		return ConstantsQueries.INSERT_PRODUCT_TYPE;
	}

	@Override
	public String getSelectQuery() {
		return ConstantsQueries.SELECT_PRODUCT_TYPE;
	}

	@Override
	public String getSelectAllQuery() {
		return ConstantsQueries.SELECT_PRODUCT_TYPES;
	}
	
	@Override
	public String getUpdateQuery() {
		return ConstantsQueries.UPDATE_PRODUCT_TYPE;
	}

	@Override
	public String getDeleteQuery() {
		return ConstantsQueries.DELETE_PRODUCT_TYPE;
	}

	@Override
	public IBean createBean(String[] values) {
		return new ProductType(values[0]);
	}

	@Override
	public IBean createBean(String[] values, long id) {
		return new ProductType(id, values[0]);
	}

}
