package by.gstu.atp.factories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.gstu.atp.beans.ProductFeature;
import by.gstu.atp.beans.ProductKind;
import by.gstu.atp.beans.ProductName;
import by.gstu.atp.beans.ProductType;
import by.gstu.atp.beans.Unit;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.impls.db.ConstantsQueries;
import by.gstu.atp.interfaces.AbstractBeanFactory;
import by.gstu.atp.interfaces.IBean;

public class ProductNameFactory extends AbstractBeanFactory {

	@Override
	public List<IBean> getBeansFromDb(PreparedStatement select)
			throws SQLException {
		List<IBean> names = new ArrayList<>();
		try (ResultSet rs = select.executeQuery()) {
			while (rs.next()) {
				try {
					names.add(getProductName(rs));
				} catch (SQLException e) {
					throw new DaoException(
							"Error reading product's name. Read names: "
									+ names.size()
									+ ". Last successfully read name: "
									+ names.get(names.size() - 1));
				}
			}
			return names;
		}
	}

	@Override
	public IBean getBeanFromDb(long id, PreparedStatement select)
			throws SQLException {
		select.setLong(1, id);
		try (ResultSet rs = select.executeQuery()) {
			if (rs.next()) {
				ProductName pn = getProductName(rs);
				pn.setCode(id);
				return pn;
			}
		}
		throw new DaoException("Product name with id = '" + id
				+ "' is not found");
	}

	private ProductName getProductName(ResultSet rs) throws SQLException {
		String name = rs.getString(1);
		String designation = rs.getString(2);
		long typeCode = rs.getLong(3);
		long kindCode = rs.getLong(4);
		long featureCode = rs.getLong(5);
		long unitCode = rs.getLong(6);
		int count = rs.getInt(7);
		String typeName = rs.getString(8);
		String kindName = rs.getString(9);
		String featureName = rs.getString(10);
		String uFullName = rs.getString(11);
		String uShortName = rs.getString(12);
		// just do it
		long id = -1L;
		try {
			id = rs.getLong(13);
		} catch (SQLException e) {
			id = -1L;
		}

		ProductType type = new ProductType(typeCode, typeName);
		ProductKind kind = new ProductKind(kindCode, kindName);
		ProductFeature feature = new ProductFeature(featureCode, featureName);
		Unit unit = new Unit(unitCode, uFullName, uShortName);

		return new ProductName(id, name, designation, type, kind, feature,
				unit, count);
	}

	@Override
	public void setBeanToDb(IBean bean, PreparedStatement insert)
			throws SQLException {
		ProductName name = (ProductName) bean;
		insert.setLong(1, name.getCode());
		insert.setString(2, name.getName());
		insert.setString(3, name.getDesignation());
		insert.setLong(4, name.getType().getCode());
		insert.setLong(5, name.getKind().getCode());
		insert.setLong(6, name.getFeature().getCode());
		insert.setLong(7, name.getUnit().getCode());
		insert.setInt(8, name.getCount());
		insert.executeUpdate();
	}

	@Override
	public void updateBeanInDb(IBean bean, PreparedStatement update)
			throws SQLException {
		ProductName name = (ProductName) bean;
		update.setString(1, name.getName());
		update.setString(2, name.getDesignation());
		update.setLong(3, name.getType().getCode());
		update.setLong(4, name.getKind().getCode());
		update.setLong(5, name.getFeature().getCode());
		update.setLong(6, name.getUnit().getCode());
		update.setInt(7, name.getCount());
		update.setLong(8, name.getCode());
		update.executeUpdate();
	}

	@Override
	public String getInsertQuery() {
		return ConstantsQueries.INSERT_PRODUCT_NAME;
	}

	@Override
	public String getSelectQuery() {
		return ConstantsQueries.SELECT_PRODUCT_NAME_ALL;
	}

	@Override
	public String getSelectAllQuery() {
		return ConstantsQueries.SELECT_PRODUCT_NAMES_ALL;
	}

	@Override
	public String getUpdateQuery() {
		return ConstantsQueries.UPDATE_PRODUCT_NAME;
	}

	@Override
	public String getDeleteQuery() {
		return ConstantsQueries.DELETE_PRODUCT_NAME;
	}

	@Override
	public IBean createBean(String[] values) {
		long code = Long.parseLong(values[0]);
		String name = values[1];
		String designation = values[2];
		ProductType type = new ProductType();
		type.setCode(Long.parseLong(values[3]));
		ProductKind kind = new ProductKind(Long.parseLong(values[4]));
		ProductFeature feature = new ProductFeature();
		feature.setCode(Long.parseLong(values[5]));
		Unit unit = new Unit();
		unit.setCode(Long.parseLong(values[6]));
		int count = Integer.parseInt(values[7]);
		return new ProductName(code, name, designation, type, kind, feature,
				unit, count);
	}

	@Override
	public IBean createBean(String[] values, long id) {
		ProductName res = (ProductName) createBean(values);
		res.setCode(id);
		return res;
	}

}
