package by.gstu.atp.factories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.gstu.atp.beans.Unit;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.impls.db.ConstantsQueries;
import by.gstu.atp.interfaces.AbstractBeanFactory;
import by.gstu.atp.interfaces.IBean;

public class UnitFactory extends AbstractBeanFactory {

	@Override
	public List<IBean> getBeansFromDb(PreparedStatement select)
			throws SQLException {
		List<IBean> units = new ArrayList<>();
		try (ResultSet rs = select.executeQuery()) {
			while (rs.next()) {
				try {
					long id = rs.getLong(1);
					String fullName = rs.getString(2);
					String shortName = rs.getString(3);
					units.add(new Unit(id, fullName, shortName));
				} catch (SQLException e) {
					throw new DaoException("Error reading unit. Read units: "
							+ units.size() + ". Last successfully read unit: "
							+ units.get(units.size() - 1));
				}
			}
			return units;
		}
	}

	@Override
	public IBean getBeanFromDb(long id, PreparedStatement select)
			throws SQLException {
		select.setLong(1, id);
		try (ResultSet rs = select.executeQuery()) {
			if (rs.next()) {
				String fullName = rs.getString(1);
				String shortName = rs.getString(2);
				return new Unit(id, fullName, shortName);
			}
		}
		throw new DaoException("Unit with id = '" + id + "' is not found");
	}

	@Override
	public void setBeanToDb(IBean bean, PreparedStatement insert)
			throws SQLException {
		Unit unit = (Unit) bean;
		insert.setString(1, unit.getFullName());
		insert.setString(2, unit.getShortName());
		insert.executeUpdate();
	}

	@Override
	public void updateBeanInDb(IBean bean, PreparedStatement update)
			throws SQLException {
		Unit unit = (Unit) bean;
		update.setString(1, unit.getFullName());
		update.setString(2, unit.getShortName());
		update.setLong(3, unit.getCode());
		update.executeUpdate();
	}

	@Override
	public String getInsertQuery() {
		return ConstantsQueries.INSERT_UNIT;
	}

	@Override
	public String getSelectQuery() {
		return ConstantsQueries.SELECT_UNIT;
	}

	@Override
	public String getSelectAllQuery() {
		return ConstantsQueries.SELECT_UNITS;
	}

	@Override
	public String getUpdateQuery() {
		return ConstantsQueries.UPDATE_UNIT;
	}

	@Override
	public String getDeleteQuery() {
		return ConstantsQueries.DELETE_UNIT;
	}

	@Override
	public IBean createBean(String[] values) {
		return new Unit(-1L, values[0], values[1]);
	}

	@Override
	public IBean createBean(String[] values, long id) {
		return new Unit(id, values[0], values[1]);
	}

}
