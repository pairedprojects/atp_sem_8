package by.gstu.atp.factories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.gstu.atp.beans.ProductFeature;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.impls.db.ConstantsQueries;
import by.gstu.atp.interfaces.AbstractBeanFactory;
import by.gstu.atp.interfaces.IBean;

public class ProductFeatureFactory extends AbstractBeanFactory {

	@Override
	public List<IBean> getBeansFromDb(PreparedStatement select)
			throws SQLException {
		List<IBean> features = new ArrayList<>();
		try (ResultSet rs = select.executeQuery()) {
			while (rs.next()) {
				try {
					long id = rs.getLong(1);
					String name = rs.getString(2);
					features.add(new ProductFeature(id, name));
				} catch (SQLException e) {
					throw new DaoException(
							"Error reading product's feature. Read features: "
									+ features.size()
									+ ". Last successfully read feature: "
									+ features.get(features.size() - 1));
				}
			}
			return features;
		}
	}

	@Override
	public IBean getBeanFromDb(long id, PreparedStatement select)
			throws SQLException {
		select.setLong(1, id);
		try (ResultSet rs = select.executeQuery()) {
			if (rs.next()) {
				String name = rs.getString(1);
				return new ProductFeature(id, name);
			}
		}
		throw new DaoException("Product feature with id = '" + id
				+ "' is not found");
	}

	@Override
	public void setBeanToDb(IBean bean, PreparedStatement insert)
			throws SQLException {
		ProductFeature feature = (ProductFeature) bean;
		insert.setString(1, feature.getName());
		insert.executeUpdate();
	}

	@Override
	public void updateBeanInDb(IBean bean, PreparedStatement update)
			throws SQLException {
		ProductFeature feature = (ProductFeature) bean;
		update.setString(1, feature.getName());
		update.setLong(2, feature.getCode());
		update.executeUpdate();
	}

	@Override
	public String getInsertQuery() {
		return ConstantsQueries.INSERT_PRODUCT_FEATURE;
	}

	@Override
	public String getSelectQuery() {
		return ConstantsQueries.SELECT_PRODUCT_FEATURE;
	}

	@Override
	public String getUpdateQuery() {
		return ConstantsQueries.UPDATE_PRODUCT_FEATURE;
	}

	@Override
	public String getDeleteQuery() {
		return ConstantsQueries.DELETE_PRODUCT_FEATURE;
	}

	@Override
	public String getSelectAllQuery() {
		return ConstantsQueries.SELECT_PRODUCT_FEATURES;
	}

	@Override
	public IBean createBean(String[] values) {
		return new ProductFeature(values[0]);
	}

	@Override
	public IBean createBean(String[] values, long id) {
		return new ProductFeature(id, values[0]);
	}

}
