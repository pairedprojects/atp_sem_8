package by.gstu.atp.factories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.gstu.atp.beans.ApplicationRate;
import by.gstu.atp.beans.Material;
import by.gstu.atp.beans.ProductFeature;
import by.gstu.atp.beans.ProductKind;
import by.gstu.atp.beans.ProductName;
import by.gstu.atp.beans.ProductType;
import by.gstu.atp.beans.Unit;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.impls.db.ConstantsQueries;
import by.gstu.atp.interfaces.AbstractBeanFactory;
import by.gstu.atp.interfaces.IBean;

public class AppRateFactory extends AbstractBeanFactory {

	@Override
	public List<IBean> getBeansFromDb(PreparedStatement select)
			throws SQLException {
		List<IBean> appRates = new ArrayList<>();
		try (ResultSet rs = select.executeQuery()) {
			while (rs.next()) {
				try {
					appRates.add(getAppRate(rs));
				} catch (SQLException e) {
					throw new DaoException(
							"Error reading app rate. Read rates: "
									+ appRates.size()
									+ ". Last successfully read app rate: "
									+ appRates.get(appRates.size() - 1));
				}
			}
			return appRates;
		}
	}

	@Override
	public IBean getBeanFromDb(long id, PreparedStatement select)
			throws SQLException {
		select.setLong(1, id);
		try (ResultSet rs = select.executeQuery()) {
			if (rs.next()) {
				ApplicationRate appRate = getAppRate(rs);
				appRate.setCode(id);
				return appRate;
			}
		}
		throw new DaoException("App rate with id = '" + id + "' is not found");
	}

	private ApplicationRate getAppRate(ResultSet rs) throws SQLException {
		// just do it
		long id = -1L;
		try {
			id = rs.getLong(1);
		} catch (SQLException e) {
			id = -1L;
		}

		long nameCode = rs.getLong(2);
		long materialCode = rs.getLong(3);
		double consumptionRate = rs.getDouble(4);
		double wasteRate = rs.getDouble(5);
		String productName = rs.getString(6);
		String designation = rs.getString(7);
		long typeCode = rs.getLong(8);
		long kindCode = rs.getLong(9);
		long featureCode = rs.getLong(10);
		long unitCode = rs.getLong(11);
		int count = rs.getInt(12);
		String typeName = rs.getString(13);
		String kindName = rs.getString(14);
		String featureName = rs.getString(15);
		String uFullName = rs.getString(16);
		String uShortName = rs.getString(17);
		String materialName = rs.getString(18);

		ProductType type = new ProductType(typeCode, typeName);
		ProductKind kind = new ProductKind(kindCode, kindName);
		ProductFeature feature = new ProductFeature(featureCode, featureName);
		Unit unit = new Unit(unitCode, uFullName, uShortName);
		Material material = new Material(materialCode, materialName, unit);
		ProductName name = new ProductName(nameCode, productName, designation,
				type, kind, feature, unit, count);

		return new ApplicationRate(id, name, material, consumptionRate,
				wasteRate);
	}

	@Override
	public void setBeanToDb(IBean bean, PreparedStatement insert)
			throws SQLException {
		ApplicationRate name = (ApplicationRate) bean;
		insert.setLong(1, name.getName().getCode());
		insert.setLong(2, name.getMaterial().getCode());
		insert.setDouble(3, name.getConsumptionRate());
		insert.setDouble(4, name.getWasteRate());
		insert.executeUpdate();
	}

	@Override
	public void updateBeanInDb(IBean bean, PreparedStatement update)
			throws SQLException {
		ApplicationRate appRate = (ApplicationRate) bean;
		update.setLong(1, appRate.getName().getCode());
		update.setLong(2, appRate.getMaterial().getCode());
		update.setDouble(3, appRate.getConsumptionRate());
		update.setDouble(4, appRate.getWasteRate());
		update.setLong(5, appRate.getCode());
		update.executeUpdate();
	}

	@Override
	public String getInsertQuery() {
		return ConstantsQueries.INSERT_APP_RATE;
	}

	@Override
	public String getSelectQuery() {
		return ConstantsQueries.SELECT_APP_RATE_ALL;
	}

	@Override
	public String getSelectAllQuery() {
		return ConstantsQueries.SELECT_APP_RATES_ALL;
	}

	@Override
	public String getUpdateQuery() {
		return ConstantsQueries.UPDATE_APP_RATE;
	}

	@Override
	public String getDeleteQuery() {
		return ConstantsQueries.DELETE_APP_RATE;
	}

	@Override
	public IBean createBean(String[] values) {
		ProductName name = new ProductName();
		name.setCode(Long.parseLong(values[0]));
		Material material = new Material();
		material.setCode(Long.parseLong(values[1]));
		double consumptionRate = Double.parseDouble(values[2]);
		double wasteRate = Double.parseDouble(values[3]);
		return new ApplicationRate(name, material, consumptionRate, wasteRate);
	}

	@Override
	public IBean createBean(String[] values, long id) {
		ApplicationRate res = (ApplicationRate) createBean(values);
		res.setCode(id);
		return res;
	}

}
