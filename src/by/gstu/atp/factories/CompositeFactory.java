package by.gstu.atp.factories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import by.gstu.atp.beans.Composite;
import by.gstu.atp.beans.ProductName;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.helpers.DatabaseUtils;
import by.gstu.atp.impls.db.ConstantsQueries;
import by.gstu.atp.interfaces.AbstractBeanFactory;
import by.gstu.atp.interfaces.IBean;

public class CompositeFactory extends AbstractBeanFactory {

	@Override
	public List<IBean> getBeansFromDb(PreparedStatement select)
			throws SQLException {
		List<IBean> comp = new ArrayList<>();
		Map<Long, String> subNames = new HashMap<>();
		try (ResultSet rs = select.executeQuery()) {
			try (Connection con = DatabaseUtils.getConnection();
					PreparedStatement selectSub = con
							.prepareStatement(ConstantsQueries.SELECT_COMPOSITE_SUB_NAMES)) {
				try (ResultSet rsSub = selectSub.executeQuery()) {
					while (rsSub.next()) {
						subNames.put(rsSub.getLong(1), rsSub.getString(2));
					}
				}
			}
			while (rs.next()) {
				try {
					Composite c = getComposite(rs);
					c.getSub().setName(subNames.get(c.getSub().getCode()));
					comp.add(c);
				} catch (SQLException e) {
					throw new DaoException(
							"Error reading composite. Read composites: "
									+ comp.size()
									+ ". Last successfully read composite: "
									+ comp.get(comp.size() - 1));
				}
			}
			return comp;
		}
	}

	@Override
	public IBean getBeanFromDb(long id, PreparedStatement select)
			throws SQLException {
		select.setLong(1, id);
		try (ResultSet rs = select.executeQuery()) {
			if (rs.next()) {
				Composite comp = getComposite(rs);
				comp.setCode(id);
				return comp;
			}
		}
		throw new DaoException("Composite with id = '" + id + "' is not found");
	}

	private Composite getComposite(ResultSet rs) throws SQLException {
		long id = rs.getLong(1);
		long mainCode = rs.getLong(2);
		String mainName = null;
		long subCode = -1;
		int count = -1;
		try {
			mainName = rs.getString(3);
			subCode = rs.getLong(4);
			count = rs.getInt(5);
		} catch (Exception e) {
			subCode = rs.getLong(3);
			count = rs.getInt(4);
		}

		ProductName main = new ProductName();
		main.setCode(mainCode);
		main.setName(mainName);
		ProductName sub = new ProductName();
		sub.setCode(subCode);

		return new Composite(id, main, sub, count);
	}

	@Override
	public void setBeanToDb(IBean bean, PreparedStatement insert)
			throws SQLException {
		Composite comp = (Composite) bean;
		insert.setLong(1, comp.getMain().getCode());
		insert.setLong(2, comp.getSub().getCode());
		insert.setInt(3, comp.getCount());
		insert.executeUpdate();
	}

	@Override
	public void updateBeanInDb(IBean bean, PreparedStatement update)
			throws SQLException {
		Composite comp = (Composite) bean;
		update.setLong(1, comp.getMain().getCode());
		update.setLong(2, comp.getSub().getCode());
		update.setInt(3, comp.getCount());
		update.setLong(4, comp.getCode());
		update.executeUpdate();
	}

	@Override
	public String getInsertQuery() {
		return ConstantsQueries.INSERT_COMPOSITE;
	}

	@Override
	public String getSelectQuery() {
		return ConstantsQueries.SELECT_COMPOSITE;
	}

	@Override
	public String getSelectAllQuery() {
		return ConstantsQueries.SELECT_COMPOSITE_ALL;
	}

	@Override
	public String getUpdateQuery() {
		return ConstantsQueries.UPDATE_COMPOSITE;
	}

	@Override
	public String getDeleteQuery() {
		return ConstantsQueries.DELETE_COMPOSITE;
	}

	@Override
	public IBean createBean(String[] values) {
		ProductName main = new ProductName();
		main.setCode(Long.parseLong(values[0]));
		ProductName sub = new ProductName();
		sub.setCode(Long.parseLong(values[1]));
		int count = Integer.parseInt(values[2]);
		return new Composite(main, sub, count);
	}

	@Override
	public IBean createBean(String[] values, long id) {
		Composite res = (Composite) createBean(values);
		res.setCode(id);
		return res;
	}

}
