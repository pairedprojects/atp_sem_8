package by.gstu.atp.factories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.gstu.atp.beans.ProductKind;
import by.gstu.atp.beans.ProductType;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.impls.db.ConstantsQueries;
import by.gstu.atp.interfaces.AbstractBeanFactory;
import by.gstu.atp.interfaces.IBean;

public class ProductKindFactory extends AbstractBeanFactory {

	@Override
	public List<IBean> getBeansFromDb(PreparedStatement select)
			throws SQLException {
		List<IBean> productKinds = new ArrayList<>();
		try (ResultSet rs = select.executeQuery()) {
			while (rs.next()) {
				try {
					long id = rs.getLong(1);
					String name = rs.getString(2);
					productKinds.add(new ProductType(id, name));
				} catch (SQLException e) {
					throw new DaoException(
							"Error reading product's kind. Read kinds: "
									+ productKinds.size()
									+ ". Last successfully read kind: "
									+ productKinds.get(productKinds.size() - 1));
				}
			}
			return productKinds;
		}
	}

	@Override
	public IBean getBeanFromDb(long id, PreparedStatement select)
			throws SQLException {
		select.setLong(1, id);
		try (ResultSet rs = select.executeQuery()) {
			if (rs.next()) {
				String name = rs.getString(1);
				return new ProductKind(id, name);
			}
		}
		throw new DaoException("Product kind with id = '" + id
				+ "' is not found");
	}

	@Override
	public void setBeanToDb(IBean bean, PreparedStatement insert)
			throws SQLException {
		ProductKind kind = (ProductKind) bean;
		insert.setString(1, kind.getName());
		insert.executeUpdate();
	}

	@Override
	public void updateBeanInDb(IBean bean, PreparedStatement update)
			throws SQLException {
		ProductKind kind = (ProductKind) bean;
		update.setString(1, kind.getName());
		update.setLong(2, kind.getCode());
		update.executeUpdate();
	}

	@Override
	public String getInsertQuery() {
		return ConstantsQueries.INSERT_PRODUCT_KIND;
	}

	@Override
	public String getSelectQuery() {
		return ConstantsQueries.SELECT_PRODUCT_KIND;
	}

	@Override
	public String getSelectAllQuery() {
		return ConstantsQueries.SELECT_PRODUCT_KINDS;
	}
	
	@Override
	public String getUpdateQuery() {
		return ConstantsQueries.UPDATE_PRODUCT_KIND;
	}

	@Override
	public String getDeleteQuery() {
		return ConstantsQueries.DELETE_PRODUCT_KIND;
	}

	@Override
	public IBean createBean(String[] values) {
		return new ProductKind(values[0]);
	}

	@Override
	public IBean createBean(String[] values, long id) {
		return new ProductKind(id, values[0]);
	}

}
