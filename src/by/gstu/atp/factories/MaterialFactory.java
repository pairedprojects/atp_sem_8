package by.gstu.atp.factories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.gstu.atp.beans.Material;
import by.gstu.atp.beans.Unit;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.impls.db.ConstantsQueries;
import by.gstu.atp.interfaces.AbstractBeanFactory;
import by.gstu.atp.interfaces.IBean;

public class MaterialFactory extends AbstractBeanFactory {

	@Override
	public List<IBean> getBeansFromDb(PreparedStatement select)
			throws SQLException {
		List<IBean> materials = new ArrayList<>();
		try (ResultSet rs = select.executeQuery()) {
			while (rs.next()) {
				try {
					String name = rs.getString(1);
					long unitCode = rs.getLong(2);
					String fullName = rs.getString(3);
					String shortName = rs.getString(4);
					long id = rs.getLong(5);

					Unit unit = new Unit(unitCode, fullName, shortName);
					materials.add(new Material(id, name, unit));
				} catch (SQLException e) {
					throw new DaoException(
							"Error reading material. Read materials: "
									+ materials.size()
									+ ". Last successfully read material: "
									+ materials.get(materials.size() - 1));
				}
			}
			return materials;
		}
	}

	@Override
	public IBean getBeanFromDb(long id, PreparedStatement select)
			throws SQLException {
		select.setLong(1, id);
		try (ResultSet rs = select.executeQuery()) {
			if (rs.next()) {
				String name = rs.getString(1);
				long unitCode = rs.getLong(2);
				String fullName = rs.getString(3);
				String shortName = rs.getString(4);

				Unit unit = new Unit(unitCode, fullName, shortName);
				return new Material(id, name, unit);
			}
		}
		throw new DaoException("Material with id = '" + id + "' is not found");
	}

	@Override
	public void setBeanToDb(IBean bean, PreparedStatement insert)
			throws SQLException {
		Material material = (Material) bean;
		insert.setLong(1, material.getCode());
		insert.setString(2, material.getName());
		insert.setLong(3, material.getUnit().getCode());
		insert.executeUpdate();
	}

	@Override
	public void updateBeanInDb(IBean bean, PreparedStatement update)
			throws SQLException {
		Material material = (Material) bean;
		update.setString(1, material.getName());
		update.setLong(2, material.getUnit().getCode());
		update.setDouble(3, material.getCode());
		update.executeUpdate();
	}

	@Override
	public String getInsertQuery() {
		return ConstantsQueries.INSERT_MATERIAL;
	}

	@Override
	public String getSelectQuery() {
		return ConstantsQueries.SELECT_MATERIAL_ALL;
	}

	@Override
	public String getSelectAllQuery() {
		return ConstantsQueries.SELECT_MATERIALS_ALL;
	}

	@Override
	public String getUpdateQuery() {
		return ConstantsQueries.UPDATE_MATERIAL;
	}

	@Override
	public String getDeleteQuery() {
		return ConstantsQueries.DELETE_MATERIAL;
	}

	@Override
	public IBean createBean(String[] values) {
		long code = Long.valueOf(values[0]);
		String name = values[1];
		Unit unit = new Unit();
		unit.setCode(Long.parseLong(values[2]));
		return new Material(code, name, unit);
	}

	@Override
	public IBean createBean(String[] values, long id) {
		Material res = (Material) createBean(values);
		res.setCode(id);
		return res;
	}

}
