package by.gstu.atp.controllers;

public final class ConstantsJSP {
	
	public static final String KEY_JSP_MESSAGE = "jspMessage";
	
	private ConstantsJSP() { }

}
