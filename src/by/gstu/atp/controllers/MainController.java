package by.gstu.atp.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gstu.atp.ConstantsPages;
import by.gstu.atp.enums.Page;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.exceptions.SkipException;
import by.gstu.atp.interfaces.AbstractController;
import by.gstu.atp.interfaces.IBean;
import by.gstu.atp.interfaces.IBeanDAO;

public class MainController extends AbstractController {

	private static final long serialVersionUID = 1L;

	@Override
	protected void performTask(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			String strPage = request.getParameter("page");
			Page reqPage = Page.convertFrom(strPage);

			HttpSession session = request.getSession();
			Page sesPage = (Page) session.getAttribute("page");

			Page page = (strPage == null && sesPage != null) ? sesPage
					: reqPage;

			IBeanDAO dao = page.getDao();
			List<IBean> beans = dao.getAll();
			request.setAttribute("beans", beans);

			session.setAttribute("page", page);
			session.setAttribute("mValues", page.getFields());

			forwardPage(ConstantsPages.JUMP_TABLES, request, response);
		} catch (SkipException e) {
			forwardPage(ConstantsPages.JUMP_MAIN, request, response);
		} catch (DaoException e) {
			jumpError(e.getMessage(), request, response);
		}
	}
}
