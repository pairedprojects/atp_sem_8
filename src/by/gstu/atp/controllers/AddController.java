package by.gstu.atp.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gstu.atp.ConstantsPages;
import by.gstu.atp.beans.HtmlField;
import by.gstu.atp.enums.Page;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.exceptions.SkipException;
import by.gstu.atp.helpers.Validator;
import by.gstu.atp.interfaces.AbstractBeanImplDB;
import by.gstu.atp.interfaces.AbstractController;
import by.gstu.atp.interfaces.IBeanDAO;

public class AddController extends AbstractController {

	private static final long serialVersionUID = 1L;

	@Override
	protected void performTask(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		HttpSession ses = request.getSession();
		Page page = (Page) ses.getAttribute("page");
		try {
			IBeanDAO dao = page.getDao();
			HtmlField[] fields = page.getFields();

			String[] values = new String[fields.length];
			for (int i = 0; i < fields.length; i++) {
				String param = request.getParameter(fields[i].getHtmlName());
				values[i] = Validator.checkReqParam(param);
			}
			AbstractBeanImplDB dbDao = (AbstractBeanImplDB) dao;
			dao.add(dbDao.getFactory().createBean(values));
			redirectPage(ConstantsPages.MAIN_CONTROLLER, response);
		} catch (SkipException e) {
			forwardPage(ConstantsPages.MAIN_CONTROLLER, request, response);
		} catch (IllegalArgumentException e) {
			jumpWarning(ConstantsPages.MAIN_CONTROLLER, e.getMessage(),
					request, response);
		} catch (DaoException e) {
			jumpError(e.getMessage(), request, response);
		}
	}
}
