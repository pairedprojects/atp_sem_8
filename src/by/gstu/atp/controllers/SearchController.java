package by.gstu.atp.controllers;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Workbook;

import by.gstu.atp.ConstantsPages;
import by.gstu.atp.beans.Composite;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.helpers.SearchUtils;
import by.gstu.atp.impls.db.ProductNameImplDB;
import by.gstu.atp.interfaces.AbstractController;
import by.gstu.atp.interfaces.IBean;
import by.gstu.atp.interfaces.IBeanDAO;

public class SearchController extends AbstractController {

	private static final long serialVersionUID = 1L;

	@Override
	protected void performTask(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			SearchUtils searcher = new SearchUtils();
			IBeanDAO dao = new ProductNameImplDB();
			HttpSession ses = request.getSession();
			List<IBean> items = dao.getAll();
			request.setAttribute("items", items);

			String strCode = request.getParameter("code");
			if (strCode != null) {
				long code = Long.parseLong(strCode);
				if (code == -1) {
					@SuppressWarnings("unchecked")
					List<Composite> list = (List<Composite>) ses
							.getAttribute("beans");
					if (list != null) {
						downloadSheet(response, searcher.formSheet(list));
					} else {
						forwardPage(ConstantsPages.JUMP_SEARCH, request,
								response);
					}
					return;
				}
				searcher.find(code);
				ses.setAttribute("beans", searcher.getFinded());
				request.setAttribute("detail", searcher.getRequired());
			}

			forwardPage(ConstantsPages.JUMP_SEARCH, request, response);
		} catch (DaoException e) {
			jumpError(e.getMessage(), request, response);
		}
	}

	private void downloadSheet(HttpServletResponse response, Workbook book) {
		final String DEFAULT_FILE_NAME = "details_total_sheet.xls";
		final String MEDIA_TYPE = "application/octet-stream";
		final String HEADER_NAME = "Content-Disposition";
		final String HEADER_CONTENT = "attachment; filename=\""
				+ DEFAULT_FILE_NAME + "\"";

		try (OutputStream os = new BufferedOutputStream(
				response.getOutputStream())) {
			response.setContentType(MEDIA_TYPE);
			response.setHeader(HEADER_NAME, HEADER_CONTENT);
			book.write(os);
			os.flush();
		} catch (IOException e) {
			throw new DaoException(e.getMessage(), e);
		}
	}
}
