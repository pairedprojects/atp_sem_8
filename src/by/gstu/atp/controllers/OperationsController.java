package by.gstu.atp.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gstu.atp.ConstantsPages;
import by.gstu.atp.enums.Operation;
import by.gstu.atp.enums.Page;
import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.helpers.Validator;
import by.gstu.atp.interfaces.AbstractController;
import by.gstu.atp.interfaces.IBean;
import by.gstu.atp.interfaces.IBeanDAO;

public class OperationsController extends AbstractController {

	private static final long serialVersionUID = 1L;

	@Override
	protected void performTask(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String strOp = request.getParameter("op");
		String[] strIds = request.getParameterValues("id");

		Operation op = Operation.convertFrom(strOp);
		IBean[] ids = Validator.checkIds(strIds);

		HttpSession session = request.getSession();
		Page page = (Page) session.getAttribute("page");

		try {
			IBeanDAO dao = page.getDao();
			switch (op) {
			case DEL:
				dao.delete(ids);
				break;

			case ADD:
				// NOP
				break;

			case EDIT:
				IBean bean = dao.get(ids[0].getCode());
				request.setAttribute("bean", bean);
				break;

			default:
				// NOP
				break;
			}

			forwardPage(ConstantsPages.MAIN_CONTROLLER, request, response);
		} catch (DaoException e) {
			jumpError(e.getMessage(), request, response);
		}
	}

}
