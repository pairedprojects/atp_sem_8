package by.gstu.atp.enums;

import by.gstu.atp.beans.HtmlField;
import by.gstu.atp.exceptions.SkipException;
import by.gstu.atp.impls.db.AppRateImplDB;
import by.gstu.atp.impls.db.CompositeImplDB;
import by.gstu.atp.impls.db.MaterialImplDB;
import by.gstu.atp.impls.db.ProductFeatureImplDB;
import by.gstu.atp.impls.db.ProductKindImplDB;
import by.gstu.atp.impls.db.ProductNameImplDB;
import by.gstu.atp.impls.db.ProductTypeImplDB;
import by.gstu.atp.impls.db.UnitImplDB;
import by.gstu.atp.interfaces.IBeanDAO;

public enum Page {

	PRODUCT_KINDS("���� ���������") {
		@Override
		public IBeanDAO getDao() {
			return new ProductKindImplDB();
		}

		@Override
		public HtmlField[] getFields() {
			return new HtmlField[] { new HtmlField("������������", "text",
					"productKind", null) };
		}
	},
	PRODUCT_TYPES("���� ���������") {
		@Override
		public IBeanDAO getDao() {
			return new ProductTypeImplDB();
		}

		@Override
		public HtmlField[] getFields() {
			return new HtmlField[] { new HtmlField("������������", "text",
					"productType", null) };
		}
	},
	PRODUCT_FEATURES("�������� ���������") {
		@Override
		public IBeanDAO getDao() {
			return new ProductFeatureImplDB();
		}

		@Override
		public HtmlField[] getFields() {
			return new HtmlField[] { new HtmlField("������������", "text",
					"productFeature", null) };
		}
	},
	PRODUCT_NAMES("������������") {
		@Override
		public IBeanDAO getDao() {
			return new ProductNameImplDB();
		}

		@Override
		public HtmlField[] getFields() {
			return new HtmlField[] {
					new HtmlField("���", "text", "productCode", null),
					new HtmlField("������������", "text", "productName", null),
					new HtmlField("�����������", "text", "productDesignation",
							null),
					new HtmlField("���", "select", "productType",
							new ProductTypeImplDB().getAll()),
					new HtmlField("���", "select", "productKind",
							new ProductKindImplDB().getAll()),
					new HtmlField("�������", "select", "productFeature",
							new ProductFeatureImplDB().getAll()),
					new HtmlField("��. ���������", "select", "productUnit",
							new UnitImplDB().getAll()),
					new HtmlField("����������", "text", "productCount", null) };
		}
	},
	MATERIALS("���������") {
		@Override
		public IBeanDAO getDao() {
			return new MaterialImplDB();
		}

		@Override
		public HtmlField[] getFields() {
			return new HtmlField[] {
					new HtmlField("���", "text", "materialCode", null),
					new HtmlField("������������", "text", "materialName", null),
					new HtmlField("��. ���������", "select", "materialUnit",
							new UnitImplDB().getAll()) };
		}
	},
	APPLICATION_RATES("����� �������") {
		@Override
		public IBeanDAO getDao() {
			return new AppRateImplDB();
		}

		@Override
		public HtmlField[] getFields() {
			return new HtmlField[] {
					new HtmlField("������������", "select", "productName",
							new ProductNameImplDB().getAll()),
					new HtmlField("��������", "select", "productMaterial",
							new MaterialImplDB().getAll()),
					new HtmlField("�������", "text", "consumptionRate", null),
					new HtmlField("������", "text", "wasteRate", null) };
		}
	},
	UNITS("������� ���������") {
		@Override
		public IBeanDAO getDao() {
			return new UnitImplDB();
		}

		@Override
		public HtmlField[] getFields() {
			return new HtmlField[] {
					new HtmlField("������ ������������", "text", "fullName",
							null),
					new HtmlField("������� ������������", "text", "shortName",
							null) };
		}
	},
	COMPOSITE("������ �������") {
		@Override
		public IBeanDAO getDao() {
			return new CompositeImplDB();
		}

		@Override
		public HtmlField[] getFields() {
			return new HtmlField[] {
					new HtmlField("�������/��", "select", "main",
							new ProductNameImplDB().getAll()),
					new HtmlField("��/�", "select", "sub",
							new ProductNameImplDB().getAll()),
					new HtmlField("���-��", "text", "count", null) };
		}
	},
	MAIN("�������") {
		@Override
		public IBeanDAO getDao() {
			throw new SkipException();
		}

		@Override
		public HtmlField[] getFields() {
			throw new SkipException();
		}
	};

	private String name;

	private Page(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public abstract IBeanDAO getDao();

	public abstract HtmlField[] getFields();

	public int getLength() {
		return getFields().length;
	}

	public static Page convertFrom(String page) {
		try {
			return Page.valueOf(page.toUpperCase());
		} catch (NullPointerException | IllegalArgumentException e) {
			return MAIN;
		}
	}

}
