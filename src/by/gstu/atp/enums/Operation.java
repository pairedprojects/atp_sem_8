package by.gstu.atp.enums;

public enum Operation {

	ADD, EDIT, DEL, NOP;

	public static Operation convertFrom(String op) {
		try {
			return Operation.valueOf(op.toUpperCase());
		} catch (RuntimeException e) {
			return NOP;
		}
	}

}
