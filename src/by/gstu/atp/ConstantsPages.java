package by.gstu.atp;

public final class ConstantsPages {

	public static final String JUMP_MAIN = "main.jsp";
	public static final String JUMP_TABLES = "tables.jsp";
	public static final String JUMP_ERROR = "error.jsp";
	public static final String JUMP_SEARCH = "search.jsp";
	
	public static final String MAIN_CONTROLLER = "main";
	
	private ConstantsPages() { }
	
}
