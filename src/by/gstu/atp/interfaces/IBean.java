package by.gstu.atp.interfaces;

public interface IBean {

	long getCode();

	String[] getHeader();

	String[] getContent();

	String getHtmlValue();

	String getHtmlName();

	String[] getFields();

}
