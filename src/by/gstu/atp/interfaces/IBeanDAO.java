package by.gstu.atp.interfaces;

import java.util.List;

public interface IBeanDAO {

	List<IBean> getAll();
	
	IBean get(long id);

	void add(IBean bean);

	void update(IBean bean);

	void delete(IBean[] beans);

}
