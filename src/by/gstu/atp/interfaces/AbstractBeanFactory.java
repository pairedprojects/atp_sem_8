package by.gstu.atp.interfaces;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractBeanFactory {

	public abstract List<IBean> getBeansFromDb(PreparedStatement select)
			throws SQLException;
	
	public abstract IBean getBeanFromDb(long id, PreparedStatement select)
			throws SQLException;

	public abstract void setBeanToDb(IBean bean, PreparedStatement insert)
			throws SQLException;

	public abstract void updateBeanInDb(IBean bean, PreparedStatement update)
			throws SQLException;

	public void deleteBeanFromDb(IBean bean, PreparedStatement delete)
			throws SQLException {
		delete.setLong(1, bean.getCode());
		delete.executeUpdate();
	}
	
	public abstract IBean createBean(String[] values);
	
	public abstract IBean createBean(String[] values, long id);

	public abstract String getInsertQuery();

	public abstract String getSelectQuery();
	
	public abstract String getSelectAllQuery();

	public abstract String getUpdateQuery();

	public abstract String getDeleteQuery();

}
