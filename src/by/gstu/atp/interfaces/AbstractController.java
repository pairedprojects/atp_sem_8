package by.gstu.atp.interfaces;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.gstu.atp.ConstantsPages;
import by.gstu.atp.controllers.ConstantsJSP;

public abstract class AbstractController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		performTask(request, response);
	}

	protected void forwardPage(String url, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		final String FOLDER_SEPARATOR = "/";

		RequestDispatcher rd = getServletContext().getRequestDispatcher(
				FOLDER_SEPARATOR + url);
		rd.forward(request, response);
	}

	protected void forwardPage(String url, String message,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute(ConstantsJSP.KEY_JSP_MESSAGE, message);
		forwardPage(url, request, response);
	}

	protected void redirectPage(String url, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect(url);
	}

	protected void jumpWarning(String url, String warning,
			HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		forwardPage(url, warning, request, response);
	}

	protected void jumpError(String message, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		forwardPage(ConstantsPages.JUMP_ERROR, message, request, response);
	}

	protected abstract void performTask(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;

}
