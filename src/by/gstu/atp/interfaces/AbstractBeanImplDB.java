package by.gstu.atp.interfaces;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import by.gstu.atp.exceptions.DaoException;
import by.gstu.atp.helpers.DatabaseUtils;

public abstract class AbstractBeanImplDB implements IBeanDAO {

	private final AbstractBeanFactory factory;

	protected AbstractBeanImplDB(AbstractBeanFactory factory) {
		this.factory = factory;
	}
	
	public AbstractBeanFactory getFactory() {
		return factory;
	}

	@Override
	public List<IBean> getAll() {
		try (Connection con = DatabaseUtils.getConnection();
				PreparedStatement select = con.prepareStatement(factory
						.getSelectAllQuery())) {
			return factory.getBeansFromDb(select);
		} catch (SQLException e) {
			System.err.println("Select bean error: " + e.getMessage());
			throw new DaoException(e.getMessage(), e);
		}
	}

	@Override
	public IBean get(final long id) {
		try (Connection con = DatabaseUtils.getConnection();
				PreparedStatement select = con.prepareStatement(factory
						.getSelectQuery())) {
			return factory.getBeanFromDb(id, select);
		} catch (SQLException e) {
			System.err.println("Select bean error: " + e.getMessage());
			throw new DaoException(e.getMessage(), e);
		}
	}

	@Override
	public void add(IBean bean) {
		try (Connection con = DatabaseUtils.getConnection();
				PreparedStatement insert = con.prepareStatement(factory
						.getInsertQuery())) {
			factory.setBeanToDb(bean, insert);
		} catch (SQLException e) {
			System.err.println("Add bean error: " + e.getMessage());
			throw new DaoException(e.getMessage(), e);
		}
	}

	@Override
	public void update(IBean bean) {
		try (Connection con = DatabaseUtils.getConnection();
				PreparedStatement update = con.prepareStatement(factory
						.getUpdateQuery())) {
			factory.updateBeanInDb(bean, update);
		} catch (SQLException e) {
			System.err.println("Update bean error: " + e.getMessage());
			throw new DaoException(e.getMessage(), e);
		}
	}

	@Override
	public void delete(IBean[] beans) {
		try (Connection con = DatabaseUtils.getConnection();
				PreparedStatement delete = con.prepareStatement(factory
						.getDeleteQuery())) {
			for (IBean bean : beans) {
				factory.deleteBeanFromDb(bean, delete);
			}
		} catch (SQLException e) {
			System.err.println("Delete bean error: " + e.getMessage());
			throw new DaoException(e.getMessage(), e);
		}
	}

}
