package by.gstu.atp.exceptions;

public class SkipException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SkipException() {
		super();
	}

	public SkipException(String message) {
		super(message);
	}

}
