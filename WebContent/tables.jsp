<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
	
<!DOCTYPE html>
<html>
<head>
<%@include file="links.html"%>
<title>
	<c:out value="${page.name}"/> - 
	Редактор НСИ &#171;Гидроблок&#187;</title>
</head>
<body>
	<header>
		<%@ include file="/parts/header.jsp"%>
		<%@ include file="/parts/menu.jsp"%>
	</header>
	<main>
		<form name="tableForm" method="GET" action="<c:url value='/operation'/>">
			<input type="hidden" name="op" value="" />
			<%@ include file="/parts/content.jsp"%>
			<%@ include file="/parts/operations.jsp"%>
		</form>
	</main>
	<%@ include file="/parts/footer.jsp"%>
	<%@ include file="/parts/modal.jsp"%>
	<%@ include file="/scripts.html"%>
</body>
</html>