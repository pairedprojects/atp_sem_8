<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html>
<head>
<%@include file="links.html"%>
<title>404</title>
</head>
<body>
	<header>
		<%@ include file="/parts/header.jsp"%>
		<%@ include file="/parts/menu.jsp"%>
	</header>
	<main> 
		<div class="container">
			<img class="responsive-img center-img" src="resources/img/404.png">
		</div>
	</main>
	<%@ include file="/parts/footer.jsp"%>
	<%@ include file="/scripts.html"%>
</body>
</html>