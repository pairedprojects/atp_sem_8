<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="links.html"%>
<title>Редактор НСИ &#171;Гидроблок&#187;</title>
</head>
<body>
	<header>
		<%@ include file="/parts/header.jsp"%>
		<%@ include file="/parts/menu.jsp"%>
	</header>
	<main> 
		<%@ include file="/parts/main-content.jsp"%>
	</main>
	<%@ include file="/parts/footer.jsp"%>
	<%@ include file="/scripts.html"%>
</body>
</html>