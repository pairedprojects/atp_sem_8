$('.dropdown-button').dropdown({
	inDuration : 300,
	outDuration : 250,
	constrain_width : true,

	hover : false,
	alignment : 'left',

	gutter : 0,
	belowOrigin : false
});

$(document).ready(function() {
	$('.slider').slider({
		full_width : true
	});
});

function checkCbForDelete() {
	var nodes = document.getElementsByTagName("INPUT");
	var flag = false;
	for (var i = 0; i < nodes.length; i++) {
		if (nodes[i].type == "checkbox" && nodes[i].checked == true) {
			flag = true;
			break;
		}
	}
	if (flag == true) {
		$("#deleteBtn").click();
	} else {
		toast("Выберите хотя бы одну запись.", 2000)
	}
}

function checkCB(op) {
	var nodes = document.getElementsByTagName("INPUT");
	var flag = false;
	for (var i = 0; i < nodes.length; i++) {
		if (nodes[i].type == "checkbox" && nodes[i].checked == true) {
			flag = true;
			break;
		}
	}
	if (flag == true) {
		setOp(op);
	} else {
		toast("Выберите хотя бы одну запись.", 2000)
	}
}

$(document).ready(function() {
	$('select').material_select();
});

$('.modal-trigger').leanModal({
	dismissible : false,
	opacity : .5,
	in_duration : 300,
	out_duration : 200,
});

$(function() {
	var top = "#toTop";
	var bottom = "#toBottom"
	var onEvent = "300";
	if ($(window).scrollTop() >= onEvent)
		$(top).fadeIn("slow")
	$(window).scroll(function() {
		if ($(window).scrollTop() <= onEvent)
			$(top).fadeOut("slow")
		else
			$(top).fadeIn("slow")
	});

	if ($(window).scrollTop() <= $(document).height() - "999")
		$(bottom).fadeIn("slow")
	$(window).scroll(function() {
		if ($(window).scrollTop() >= $(document).height() - "999")
			$(bottom).fadeOut("slow")
		else
			$(bottom).fadeIn("slow")
	});

	$(top).click(function() {
		$("html,body").animate({
			scrollTop : 0
		}, "slow")
	})
	$(bottom).click(function() {
		$("html,body").animate({
			scrollTop : $(document).height()
		}, "slow")
	})
});

var isChecked = true;
function selectCB() {
	var nodes = document.getElementsByTagName("INPUT");
	for (var i = 0; i < nodes.length; i++) {
		if (nodes[i].type == "checkbox")
			nodes[i].checked = isChecked;
	}
	isChecked = !isChecked;
}

function setPage(page) {
	document.setPageForm.page.value = page;
	document.setPageForm.submit();
}

function setOp(op) {
	document.tableForm.op.value = op;
	document.tableForm.submit();
}

function setDetail(detail) {
	document.detailForm.code.value = detail;
	document.detailForm.submit();
}