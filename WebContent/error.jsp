<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html>
<head>
<%@include file="links.html"%>
<title>Ошибка</title>
</head>
<body>
	<header>
		<%@ include file="/parts/header.jsp"%>
		<%@ include file="/parts/menu.jsp"%>
	</header>
	<main> 
		<div class="container">
			<h5>${jspMessage}</h5>
		</div>
	</main>
	<%@ include file="/parts/footer.jsp"%>
	<%@ include file="/scripts.html"%>
</body>
</html>