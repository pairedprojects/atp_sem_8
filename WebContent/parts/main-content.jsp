<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="container main-container-fix">
	<div class="slider slider-fix ">
		<ul class="slides">
			<li><img src="resources/img/free_time.jpg">
				<div class="caption center-align caption-right">
					<h3 class="white-text text-accent-2">Всегда вовремя</h3>
				</div></li>
			<li><img src="resources/img/graph.jpg">
				<div class="caption left-align caption-left black-text">
					<h3>Быстро и эффективно</h3>
				</div></li>
			<li><img src="resources/img/acces.jpg">
				<div class="caption center-align">
					<h2>Доступ в любом месте</h2>
				</div></li>
		</ul>
	</div>
</div>