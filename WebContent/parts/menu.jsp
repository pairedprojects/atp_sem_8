<%@page import="by.gstu.atp.enums.Page"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="container">
	<a href="#" data-activates="nav-mobile" class="button-collapse top-nav"><i
		class="mdi-navigation-menu"></i></a>
</div>
<ul id="nav-mobile" class="side-nav fixed">
	<li class="logo logo-fix"><a id="logo-container-fix" href="JavaScript: setPage('<%=Page.MAIN%>')"
		class="brand-logo" href="#"><img src="resources/icons/logo.png"
			height="100"></a></li>
	<li class="no-padding">
		<ul class="collapsible collapsible-accordion">
			<li class="bold"><a
				class="collapsible-header  waves-effect waves-teal menu-font-fix">Справочники</a>
				<div class="collapsible-body">
					<ul>
						<li><a href="JavaScript: setPage('<%=Page.PRODUCT_KINDS%>')">Виды продукции</a></li>
						<li><a href="JavaScript: setPage('<%=Page.PRODUCT_TYPES%>')">Типы продукции</a></li>
						<li><a href="JavaScript: setPage('<%=Page.PRODUCT_FEATURES%>')">Признаки продукции</a></li>
						<li><a href="JavaScript: setPage('<%=Page.PRODUCT_NAMES%>')">Наименования</a></li>
						<li><a href="JavaScript: setPage('<%=Page.MATERIALS%>')">Материалы</a></li>
						<li><a href="JavaScript: setPage('<%=Page.APPLICATION_RATES%>')">Нормы расхода</a></li>
						<li><a href="JavaScript: setPage('<%=Page.UNITS%>')">Единицы измерения</a></li>
					</ul>
				</div></li>
			<li class="bold"><a
				class="collapsible-header  waves-effect waves-teal menu-font-fix">Разузлование</a>
				<div class="collapsible-body">
					<ul>
						<li><a href="JavaScript: setPage('<%=Page.COMPOSITE%>')">Состав изделий</a></li>
						<li><a href="<c:url value='/search-detail'/>">Поиск</a></li>
					</ul>
				</div></li>
		</ul>
	</li>
</ul>
<form name="setPageForm" method="GET" action="<c:url value='/main'/>">
	<input type="hidden" name="page" value="" />
</form>
