﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="container">
	<h3>
		<c:out value="${page.name}" />
	</h3>
	<table class="bordered hoverable table-fix">
		<thead>
			<tr>
				<th style="width: 2%"><a href="JavaScript: selectCB()"
					class="black-text" title="Выбрать все"><i
						class="small mdi-action-spellcheck"></i></a></th>
				<c:forEach var="headItem" items="${beans[0].header}">
					<th><c:out value="${headItem}" /></th>
				</c:forEach>
			</tr>
		</thead>
		<c:forEach var="bean" items="${beans}">
			<tr>
				<td><input type="checkbox" id="${bean.code}" name="id"
					value="${bean.code}" /><label for="${bean.code}"></label></td>
				<c:forEach var="contentItem" items="${bean.content}">
					<td><c:out value="${contentItem}" /></td>
				</c:forEach>
			</tr>
		</c:forEach>
	</table>
</div>