<%@page import="by.gstu.atp.enums.Page"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fn.tld" prefix="fn"%>

<c:if test="${not empty jspMessage}">
	<script>
		window.onload = function() {
			$("#addBtn").click();
		}
	</script>
</c:if>

<c:if test="${not empty bean}">
	<script>
		window.onload = function() {
			$("#editBtn").click();
		}
	</script>
</c:if>

<div id="addModal" class="modal">
	<div class="modal-header green">
		<div class="container modal-container-fix white-text">
			<h5>Добавление записи</h5>
		</div>
	</div>
	<div class="modal-content">
		<div class="row">
			<c:if test="${not empty jspMessage}">
				<p class="alert alert-error">
					<c:out value="${jspMessage}" />
				</p>
			</c:if>
			<form class="col s12" id="addFormId" name="addForm" method="POST"
				action="<c:url value='/add'/>">
				<div class="col s6">
					<c:forEach var="htmValue" items="${mValues}">
						<c:if test="${htmValue.type eq 'text'}">
							<div class="input-field col s12 s6-fix">
								<input id="${htmValue.name}" name="${htmValue.htmlName}"
									type="text" class="validate" /> <label for="${htmValue.name}">${htmValue.name}</label>
							</div>
						</c:if>
					</c:forEach>
				</div>
				<div class="col s6">
					<c:forEach var="htmValue" items="${mValues}">
						<c:if test="${htmValue.type eq 'select'}">
							<div class="input-field col s12 s6-fix">
								<select name="${htmValue.htmlName}">
									<option value="" disabled selected>${htmValue.name}</option>
									<c:forEach var="option" items="${htmValue.values}">
										<option value="${option.htmlValue}">${option.htmlName}</option>
									</c:forEach>
								</select>
							</div>
						</c:if>
					</c:forEach>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer modal-footer-fix">
		<a class="waves-effect waves-red btn-flat modal-action modal-close">Отмена</a>
		<button onclick="document.addForm.submit()"
			class="waves-effect waves-green btn-flat modal-action">Добавить</button>
	</div>
</div>

<div id="editModal" class="modal">
	<div class="modal-header orange">
		<div class="container modal-container-fix white-text">
			<h5>Редактирование записи</h5>
		</div>
	</div>
	<div class="modal-content">
		<div class="row">
			<c:if test="${not empty jspMessage}">
				<p class="alert alert-error">
					<c:out value="${jspMessage}" />
				</p>
			</c:if>
			<form class="col s12" name="editForm" method="POST"
				action="<c:url value='/edit'/>">
				<input type="hidden" name="beanId" value="${bean.code}">
				<div class="col s6">
					<c:forEach var="i" begin="0" end="${page.length - 1}">
						<c:if test="${mValues[i].type eq 'text'}">
							<div class="input-field col s12 s6-fix">
								<input id="${mValues[i].name}" name="${mValues[i].htmlName}"
									type="text" class="validate" value="${bean.fields[i]}" /> <label
									for="${mValues[i].name}">${mValues[i].name}</label>
							</div>
						</c:if>
					</c:forEach>
				</div>
				<div class="col s6">
					<c:forEach var="i" begin="0" end="${page.length - 1}">
						<c:if test="${mValues[i].type eq 'select'}">
							<div class="input-field col s12 s6-fix">
								<select name="${mValues[i].htmlName}">
									<option value="" disabled>${mValues[i].name}</option>
									<c:forEach var="option" items="${mValues[i].values}">
										<c:set var="pValue" value="${fn:trim(option.htmlValue)}" />
										<c:set var="bValue" value="${fn:trim(bean.fields[i])}" />
										<c:if test="${pValue ne bValue}">
											<option value="${option.htmlValue}">${option.htmlName}</option>
										</c:if>
										<c:if test="${pValue eq bValue}">
											<option value="${option.htmlValue}" selected>${option.htmlName}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</c:if>
					</c:forEach>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer modal-footer-fix">
		<a class="waves-effect waves-red btn-flat modal-action modal-close">Отмена</a>
		<button onclick="document.editForm.submit()"
			class="waves-effect waves-green btn-flat modal-action">Сохранить</button>
	</div>
</div>

<div id="deleteModal" class="modal">
	<div class="modal-header red">
		<div class="container modal-container-fix white-text">
			<h5>Удаление записи</h5>
		</div>
	</div>
	<div class="modal-content container">
		<p class="modal-text">
			Вы действительно хотите удалить запись(и)?
		</p>
	</div>
	<div class="modal-footer modal-footer-fix">
		<a class="waves-effect waves-red btn-flat modal-action modal-close">Нет</a>
		<button onclick="JavaScript: checkCB('del')"
			class="waves-effect waves-green btn-flat modal-action">Да</button>
	</div>
</div>