﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container">
			© 2014-2015 ГГТУ ИТ-42, Егор Семенченя & Максим Потапенко<a class="grey-text text-lighten-4 right"
				href="https://bitbucket.org/pairedprojects/atp_sem_8">Bitbucket</a>
		</div>
	</div>
</footer>