<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div id="toTop" class="go-button go-up" title="Вверх">
	<a
		class="btn-floating btn-large waves-effect waves-light  pink accent-3"><i
		class="large mdi-hardware-keyboard-arrow-up"></i></a>
</div>

<div class="optional-panel">
	<div class="row row-fix" title="Добавить запись">
		<a id="addBtn"
			class="btn-floating btn-large waves-effect waves-light green modal-trigger"
			href="#addModal"><i class="large mdi-content-add"></i></a>
	</div>

	<div class="row row-fix" title="Изменить запись">
		<a
			class="btn-floating btn-large waves-effect waves-light orange modal-trigger"
			href="JavaScript: checkCB('edit')"><i
			class="large mdi-editor-mode-edit"></i></a> <a id="editBtn"
			class="modal-trigger" href="#editModal"></a>
	</div>

	<div class="row row-fix" title="Удалить запись">
		<a href="JavaScript: checkCbForDelete()"
			class="btn-floating btn-large waves-effect waves-light red modal-trigger"><i
			class="large mdi-content-clear"></i></a> <a id="deleteBtn"
			class="modal-trigger" href="#deleteModal"></a>
	</div>
</div>

<div id="toBottom" class="go-button go-down" title="Вниз">
	<a
		class="btn-floating btn-large waves-effect waves-light  pink accent-3"><i
		class="large mdi-hardware-keyboard-arrow-down"></i></a>
</div>

