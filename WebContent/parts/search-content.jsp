<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="container">
	<h3>
		<br /> Поиск бракованной детали
	</h3>

	<ul id="details" class="dropdown-content dropdown-content-fix">
		<c:forEach var="item" items="${items}">
			<li><a href="JavaScript: setDetail('${item.code}')"
				id="${item.name}">${item.htmlName}<span class="badge">${item.code}</span></a></li>
		</c:forEach>
	</ul>
	<a class="btn dropdown-button pink darken-3" style="width: 500px;"
		href="#!" data-activates="details">Наименование детали <i
		class="mdi-navigation-arrow-drop-down right"></i>
	</a>

	<c:if test="${not empty beans}">
		<table class="bordered hoverable table-fix">
			<thead>
				<tr>
					<th>Наименование СЕ/Изделия</th>
					<th>Количество деталей <c:out value="${detail.name}" />,
						входящих в СЕ/Изделие
					</th>
				</tr>
			</thead>
			<c:forEach var="bean" items="${beans}">
				<tr>
					<td><c:out value="${bean.main.name}" /></td>
					<td><c:out value="${bean.count}" /></td>
				</tr>
			</c:forEach>
		</table>

		<br />
		<a class="waves-effect waves-light pink darken-3 btn right"
			href="JavaScript: setDetail('-1')"><i
			class="mdi-action-description right"></i>Получить ведомость</a>
	</c:if>
</div>

<form name="detailForm" method="GET"
	action="<c:url value='/search-detail'/>">
	<input type="hidden" name="code" value="" />
</form>
